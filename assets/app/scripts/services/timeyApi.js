'use strict';

angular.module('timeyApi', ['ngResource'])
  .factory('Api', function ($resource, $http) {
    var Api = $resource(
        'http://timey.taronfoxworth.com/api/v1/task/:Id', 
        {Id: "@Id" },
        { 
            save : {
                method : 'GET',
                url : '/api/v1/task/create'
            },
            update : {
                method : 'PUT',
            }
        }
      );
    
    return Api;
});
