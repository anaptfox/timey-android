'use strict';

angular.module('timeyApp')
  .controller('EditCtrl', function ($scope,$location,  Api, $http, dateFilter, $routeParams) {
    
    console.log("sef")
    Api.get({Id: $routeParams.id}, function(data) {
      self.original = data.task[0];
      $scope.task = new Api(self.original);
    });
   
    $scope.isClean = function() {
      return angular.equals(self.original, $scope.task);
    }
   
    console.log(Api.save);
    $scope.save = function(taskId) {
      $http({
        url: "http://timey.taronfoxworth.com/api/v1/task/"+taskId+"/edit", 
        method: "GET",
        params: $scope.task
     }).success(function(success){
       $location.path('/');
     });
    }
    $scope.deleteTask = function(taskId){
      Api.delete({Id : taskId }, function(success){
          $location.path('/');
      });
    }

});
