'use strict';

angular.module('timeyApp')
  .controller('NewCtrl', function ($scope, Api, $http, dateFilter, $location) {
    
    var state = 'stop';


    $scope.task = {
      name:"",
      description: "",
      startTime: "Please Press Start to begin",
      endTime: "Pleas Press Stop to end"
    }


    $scope.start = function(){
      state = 'start';
      this.task.startTime = dateFilter(new Date(), 'M/d/yy h:mm:ss a');
    }

    $scope.stop = function(){
      this.task.endTime = dateFilter(new Date(), 'M/d/yy h:mm:ss a');
      Api.save(this.task, function(success){
        $location.path('/');
      });
      
    }

    $scope.hasStarted = function(){
      if (state == 'start'){
        return false;
      }else{
        return true;
      }
    }

});
