'use strict';

angular.module('timeyApp', ['timeyApi', 'timeyTime'])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      }).when('/new', {
        templateUrl: 'views/new.html',
        controller: 'NewCtrl'
      }).when('/edit/:id', {
        templateUrl: 'views/edit.html',
        controller: 'EditCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
});
